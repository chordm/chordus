import { Typography } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';

const meta: Meta<typeof Typography> = {
  component: Typography,
  title: 'UI Kit/Typography',
};

export default meta;

type Story = StoryObj<typeof Typography>;

export const Demo: Story = {
  render: () => (
    <>
      <Typography variant="h1">h1</Typography>
      <Typography variant="h2">h2</Typography>
      <Typography variant="h3">h3</Typography>
      <Typography variant="h4">h4</Typography>
      <Typography variant="body1">body1</Typography>
      <Typography variant="body1">body1</Typography>
      <div>
        <Typography variant="bold">bold</Typography>
      </div>
      <Typography variant="caption">caption</Typography>
      <br />
      <Typography variant="h1">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad, dolorem
        perferendis incidunt quisquam saepe optio minus repudiandae ipsam dolore
        numquam molestiae magnam. Laudantium, aperiam. Consequatur ipsum sapient
      </Typography>
      <Typography variant="h2">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad, dolorem
        perferendis incidunt quisquam saepe optio minus repudiandae ipsam dolore
        numquam molestiae magnam. Laudantium, aperiam. Consequatur ipsum
      </Typography>
      <Typography variant="h3">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad, dolorem
        perferendis incidunt quisquam saepe optio minus repudiandae ipsam dolore
        numquam molestiae magnam. Laudantium, aperiam. Consequatur ipsum
      </Typography>
      <Typography variant="h4">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad, dolorem
        perferendis incidunt quisquam saepe optio minus repudiandae ipsam dolore
        numquam molestiae magnam. Laudantium, aperiam. Consequatur ipsum
        sapiente eaque animi! Vitae voluptatem alias maxime sunt cumque eum
        exercitationem nisi fugiat tempora temporibus veritatis quisquam, nulla
        explicabo quae, laborum iusto suscipit ducimus vero nesciunt cum. Ipsa
        sit modi assumenda dolorem earum. Magnam, exercitationem. Dicta voluptas
        placeat id nisi itaque eos nesciunt debitis excepturi blanditiis
      </Typography>
      <Typography variant="body1">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad, dolorem
        perferendis incidunt quisquam saepe optio minus repudiandae ipsam dolore
        numquam molestiae magnam. Laudantium, aperiam. Consequatur ipsum
        sapiente eaque animi! Vitae voluptatem alias maxime sunt cumque eum
        exercitationem nisi fugiat tempora temporibus veritatis quisquam, nulla
        explicabo quae, laborum iusto suscipit ducimus vero nesciunt cum. Ipsa
        sit modi assumenda dolorem earum. Magnam, exercitationem. Dicta voluptas
        placeat id nisi itaque eos nesciunt debitis excepturi blanditiis
        eveniet? Voluptatum incidunt numquam aliquid, molestiae blanditiis quasi
        nihil, inventore odio laudantium totam nulla cupiditate vero minima.
        Beatae maiores iusto quasi natus laborum est optio ipsum praesentium, id
        rerum ullam, error dignissimos labore repellat provident atque inventore
        rem ex suscipit at fugiat earum? Fuga obcaecati voluptates qui? Ullam
        quam facilis ad iusto reprehenderit sed fugiat neque consequatur vero
        reiciendis aspernatur quae deserunt, nam placeat voluptates natus
        delectus aperiam repudiandae repellat. Repellat odit possimus quidem
        ducimus! Sit, excepturi!
      </Typography>
      <Typography variant="body1">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad, dolorem
        perferendis incidunt quisquam saepe optio minus repudiandae ipsam dolore
        numquam molestiae magnam. Laudantium, aperiam. Consequatur ipsum
        sapiente eaque animi! Vitae voluptatem alias maxime sunt cumque eum
        exercitationem nisi fugiat tempora temporibus veritatis quisquam, nulla
        explicabo quae, laborum iusto suscipit ducimus vero nesciunt cum. Ipsa
        sit modi assumenda dolorem earum. Magnam, exercitationem. Dicta voluptas
        placeat id nisi itaque eos nesciunt debitis excepturi blanditiis
        eveniet? Voluptatum incidunt numquam aliquid, molestiae blanditiis quasi
        nihil, inventore odio laudantium totam nulla cupiditate vero minima.
        Beatae maiores iusto quasi natus laborum est optio ipsum praesentium, id
        rerum ullam, error dignissimos labore repellat provident atque inventore
        rem ex suscipit at fugiat earum? Fuga obcaecati voluptates qui? Ullam
        quam facilis ad iusto reprehenderit sed fugiat neque consequatur vero
        reiciendis aspernatur quae deserunt, nam placeat voluptates natus
        delectus aperiam repudiandae repellat. Repellat odit possimus quidem
        ducimus! Sit, excepturi!
      </Typography>
      <div>
        <Typography variant="bold">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad,
          dolorem perferendis incidunt quisquam saepe optio minus repudiandae
          ipsam dolore numquam molestiae magnam. Laudantium, aperiam.
          Consequatur ipsum sapiente eaque animi! Vitae voluptatem alias maxime
          sunt cumque eum exercitationem nisi fugiat tempora temporibus
          veritatis quisquam, nulla explicabo quae, laborum iusto suscipit
          ducimus vero nesciunt cum. Ipsa sit modi assumenda dolorem earum.
          Magnam, exercitationem. Dicta voluptas placeat id nisi itaque eos
          nesciunt debitis excepturi blanditiis eveniet? Voluptatum incidunt
          numquam aliquid, molestiae blanditiis quasi nihil, inventore odio
          laudantium totam nulla cupiditate vero minima. Beatae maiores iusto
          quasi natus laborum est optio ipsum praesentium, id rerum ullam, error
          dignissimos labore repellat provident atque inventore rem ex suscipit
          at fugiat earum? Fuga obcaecati voluptates qui? Ullam quam facilis ad
          iusto reprehenderit sed fugiat neque consequatur vero reiciendis
          aspernatur quae deserunt, nam placeat voluptates natus delectus
          aperiam repudiandae repellat. Repellat odit possimus quidem ducimus!
          Sit, excepturi!
        </Typography>
      </div>
      <Typography variant="caption">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id ad, dolorem
        perferendis incidunt quisquam saepe optio minus repudiandae ipsam dolore
        numquam molestiae magnam. Laudantium, aperiam. Consequatur ipsum
        sapiente eaque animi! Vitae voluptatem alias maxime sunt cumque eum
        exercitationem nisi fugiat tempora temporibus veritatis quisquam, nulla
        explicabo quae, laborum iusto suscipit ducimus vero nesciunt cum. Ipsa
        sit modi assumenda dolorem earum. Magnam, exercitationem. Dicta voluptas
        placeat id nisi itaque eos nesciunt debitis excepturi blanditiis
        eveniet? Voluptatum incidunt numquam aliquid, molestiae blanditiis quasi
        nihil, inventore odio laudantium totam nulla cupiditate vero minima.
        Beatae maiores iusto quasi natus laborum est optio ipsum praesentium, id
        rerum ullam, error dignissimos labore repellat provident atque inventore
        rem ex suscipit at fugiat earum? Fuga obcaecati voluptates qui? Ullam
        quam facilis ad iusto reprehenderit sed fugiat neque consequatur vero
        reiciendis aspernatur quae deserunt, nam placeat voluptates natus
        delectus aperiam repudiandae repellat. Repellat odit possimus quidem
        ducimus! Sit, excepturi!
      </Typography>
    </>
  ),
};

export const H1: Story = {
  name: 'h1',
  args: {
    children: 'h1',
    variant: 'h1',
  },
};

export const H2: Story = {
  name: 'h2',
  args: {
    children: 'h2',
    variant: 'h2',
  },
};

export const H3: Story = {
  name: 'h3',
  args: {
    children: 'h3',
    variant: 'h3',
  },
};

export const H4: Story = {
  name: 'h4',
  args: {
    children: 'h4',
    variant: 'h4',
  },
};

export const Body1: Story = {
  name: 'body1',
  args: {
    children: 'body1',
    variant: 'body1',
  },
};

export const Body2: Story = {
  name: 'body2',
  args: {
    children: 'body2',
    variant: 'body2',
  },
};

export const Bold: Story = {
  name: 'bold',
  args: {
    children: 'bold',
    variant: 'bold',
  },
};

export const Caption: Story = {
  name: 'caption',
  args: {
    children: 'caption',
    variant: 'caption',
  },
};
