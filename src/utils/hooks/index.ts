import {
  Dispatch,
  RefObject,
  SetStateAction,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';

export const useOnClickOutside = <T extends HTMLElement>(
  ref: RefObject<T>,
  handler: (...args: unknown[]) => unknown,
) => {
  useEffect(() => {
    const listener = (event: Event) => {
      if (!ref.current || ref.current.contains(event.target as Node)) {
        return;
      }

      handler(event);
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
};

export const useStateForNestedArray = <T extends E[], E>(
  state: T,
  setState: Dispatch<SetStateAction<E[]>>,
): [(index: number) => E, (index: number) => Dispatch<SetStateAction<E>>] => {
  const getStateForIndex = useCallback(
    (index: number) => state[index],
    [state],
  );
  const setStateForIndex = useCallback(
    (index: number) => (arg: SetStateAction<E>) => {
      setState(state => {
        const itemState = arg instanceof Function ? arg(state[index]) : arg;
        return [...state.slice(0, index), itemState, ...state.slice(index + 1)];
      });
    },
    [setState],
  );

  return [getStateForIndex, setStateForIndex];
};

export type UseBooleanActions = {
  set: React.Dispatch<SetStateAction<boolean>>;
  toggle: () => void;
  on: () => void;
  off: () => void;
};
export type UseBoolean = [boolean, UseBooleanActions];

export const useBoolean = (initial = false): UseBoolean => {
  const [value, set] = useState<boolean>(initial);
  const toggle = useCallback(() => set(v => !v), []);
  const on = useCallback(() => set(true), []);
  const off = useCallback(() => set(false), []);
  const actions = useMemo(() => ({ set, toggle, on, off }), [off, on, toggle]);
  return useMemo(() => [value, actions], [actions, value]);
};

export const useDebounce = <Callback extends (...args: any[]) => any>(
  callback: Callback,
  wait: number,
) => {
  const timeout = useRef<NodeJS.Timeout>();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const safeCallback = useCallback(callback, []);

  const firingCallback = useCallback(
    (...props: Parameters<typeof safeCallback>) => {
      if (timeout.current) {
        clearTimeout(timeout.current);
      }
      timeout.current = setTimeout(() => safeCallback(...props), wait);
    },
    [safeCallback, wait],
  );

  return firingCallback;
};

export const useThrottle = (callback: () => unknown, wait: number) => {
  const isCalled = useRef(false);

  const firingCallback = (...props: Parameters<typeof callback>) => {
    if (isCalled.current) return;
    isCalled.current = true;

    setTimeout(async () => {
      await callback(...props);
      isCalled.current = false;
    }, wait);
  };

  return firingCallback;
};

export const useIsOnScreen = (
  ref: RefObject<Element>,
  options: IntersectionObserverInit = {},
) => {
  const [isIntersecting, setIntersecting] = useState(false);
  const isClient = typeof window !== 'undefined';

  let observer: IntersectionObserver;

  if (isClient) {
    observer = new IntersectionObserver(
      ([entry]) => setIntersecting(entry.isIntersecting),
      { rootMargin: '0px 0px 50% 0px', ...options },
    );
  }

  useEffect(() => {
    if (!isClient || !ref.current) return;
    observer.observe(ref.current);
    return () => observer.disconnect();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return isIntersecting;
};

export const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState<{
    width?: number;
    height?: number;
  }>({
    width: undefined,
    height: undefined,
  });

  const handleResize = useCallback(() => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  }, []);

  const handleResizeDebounced = useDebounce(handleResize, 250);

  useEffect(() => {
    window.addEventListener('resize', handleResizeDebounced);
    handleResizeDebounced();
    return () => window.removeEventListener('resize', handleResizeDebounced);
  }, [handleResizeDebounced]);

  return windowSize;
};
