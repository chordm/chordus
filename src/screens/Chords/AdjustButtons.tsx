import { Box, Button, Typography, styled } from '@mui/material';
import PlusIcon from '@/public/icons/plus.svg';
import MinusIcon from '@/public/icons/minus.svg';
import { FC, PropsWithChildren } from 'react';

const StyledButton = styled(Button)({
  padding: '5px',
  minWidth: 'unset',
  borderRadius: 4,
});

interface AdjustButtonsProps extends PropsWithChildren {
  onPlus?: () => void;
  onMinus?: () => void;
}

export const AdjustButtons: FC<AdjustButtonsProps> = ({
  onMinus,
  onPlus,
  children,
}) => (
  <Box sx={{ display: 'flex', alignItems: 'center', gap: 5 }}>
    <StyledButton onClick={onMinus} variant="outlined">
      <MinusIcon />
    </StyledButton>
    <Typography>{children}</Typography>
    <StyledButton onClick={onPlus} variant="outlined">
      <PlusIcon />
    </StyledButton>
  </Box>
);
