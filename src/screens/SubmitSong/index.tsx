'use client';

import { ChordPanel } from 'components/ChordPanel';
import { RhythmInput } from 'components/RhythmInput';
import { Autocomplete } from 'components/Autocomplete';
import { rhythmLinesMocks } from 'mocks/rhythmLines';
import { wordLinesMocks } from 'mocks/wordLines';
import {
  FC,
  SyntheticEvent,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { ChordPanels, Performer, RhythmPanel, RhythmPanels, Song } from 'types';
import { generateRhythm } from 'utils/chords';
import { useStateForNestedArray } from 'utils/hooks';
import API from 'api';
import { AUTOCOMPLETE_DEFAULT_ITEMS_SIZE } from 'constant';
import { GeneralValidation } from './GeneralValidation';
import { validationSchema } from 'screens/SubmitSong/validation';
import { useParams } from 'next/navigation';
import {
  AutocompleteChangeReason,
  AutocompleteRenderInputParams,
  Avatar,
  Box,
  Divider,
  FormControl,
  MenuItem,
  OutlinedInput,
  OutlinedInputProps,
  Typography,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';

import { useCreateSong, useSong, useUpdateSong } from 'queries/songs';
import { CreateSongDto, FetchedPerformer, FetchedSong } from 'api/dtos';
import { CenterDesktopLayout } from 'layouts/DesktopLayout/CenterDesktopLayout';
import { Breadcrumbs, links } from 'components/Breadcrumbs';
import CameraIcon from '@/public/icons/camera.svg';

import { DndProvider } from 'react-dnd-multi-backend';
import { HTML5toTouch } from 'rdndmb-html5-to-touch'; // or any other pipeline

export const SubmitSong: FC = () => {
  const routerParams = useParams();
  const songId: number | undefined = useMemo(() => {
    const rawId = routerParams.index?.[0];
    return typeof rawId === 'undefined' ? rawId : Number(rawId);
  }, [routerParams]);
  const song = useSong(songId);
  const [rhythm, setRhythm] = useState<[number, number]>([4, 4]);
  const [titles, setTitles] = useState<string[]>([]);
  const defaultWordPanel = useRef([[]]);
  const defaultRhythmPanel = useRef([[generateRhythm(rhythm[0])]]);
  const [songTitle, setSongTitle] = useState('');
  const [performerBandName, setPerformerBandName] = useState('');

  const [wordPanels, setWordPanels] = useState<ChordPanels>([
    wordLinesMocks,
    defaultWordPanel.current,
  ]);
  const [rhythmPanels, setRhythmPanels] = useState<RhythmPanels>([
    rhythmLinesMocks,
    defaultRhythmPanel.current,
  ]);
  const [getWordsForIndex, setWordsForIndex] = useStateForNestedArray(
    wordPanels,
    setWordPanels,
  );
  const [getRhythmForIndex, setRhythmForIndex] = useStateForNestedArray(
    rhythmPanels,
    setRhythmPanels,
  );
  const [getTitleForIndex, setTitleForIndex] = useStateForNestedArray(
    titles,
    setTitles,
  );

  useEffect(() => {
    if (!song.data) return;
    setRhythm(song.data.rhythm);
    setTitles(song.data.titles);
    setSongTitle(song.data.songTitle);
    setPerformerBandName(song.data.performer.bandName);
    setWordPanels(song.data.chordPanels);
    setRhythmPanels(song.data.rhythmPanels);
  }, [song.data]);

  const removePanelForIndex = useCallback(
    (index: number) => () => {
      setWordPanels(wordPanels => [
        ...wordPanels.slice(0, index),
        ...wordPanels.slice(index + 1),
      ]);
      setRhythmPanels(rhythmPanels => [
        ...rhythmPanels.slice(0, index),
        ...rhythmPanels.slice(index + 1),
      ]);
      setTitles(titles => [
        ...titles.slice(0, index),
        ...titles.slice(index + 1),
      ]);
    },
    [],
  );
  const insertNewPanelForIndex = useCallback(
    (index: number) => () => {
      setWordPanels(wordPanels => [
        ...wordPanels.slice(0, index + 1),
        defaultWordPanel.current,
        ...wordPanels.slice(index + 1),
      ]);
      setRhythmPanels(rhythmPanels => [
        ...rhythmPanels.slice(0, index + 1),
        defaultRhythmPanel.current,
        ...rhythmPanels.slice(index + 1),
      ]);
    },
    [],
  );

  const createSong = useCreateSong();
  const updateSong = useUpdateSong();

  const onSubmitSong = useCallback(async () => {
    if (typeof songId === 'undefined') {
      throw new Error('No song to submit');
    }
    const songDto: CreateSongDto = {
      chordPanels: wordPanels,
      rhythmPanels: rhythmPanels,
      titles,
      rhythm,
      performerBandName,
      songTitle,
    };

    song.data
      ? updateSong.mutateAsync({ songId, songDto })
      : createSong.mutateAsync(songDto);
  }, [
    createSong,
    performerBandName,
    rhythm,
    rhythmPanels,
    song.data,
    songId,
    songTitle,
    titles,
    updateSong,
    wordPanels,
  ]);

  const generatePanel = useCallback(
    (_value: RhythmPanel, index: number, array: RhythmPanel[]) => (
      <ChordPanel
        key={index}
        rhythmLines={getRhythmForIndex(index)}
        wordLines={getWordsForIndex(index)}
        setRhythmLines={setRhythmForIndex(index)}
        setWordLines={setWordsForIndex(index)}
        title={getTitleForIndex(index)}
        setTitle={setTitleForIndex(index)}
        onDelete={removePanelForIndex(index)}
        onInsertNew={insertNewPanelForIndex(index)}
        rhythm={rhythm}
        isEditing
        disablePanelRemoving={array.length < 2}
      />
    ),
    [
      getRhythmForIndex,
      getTitleForIndex,
      getWordsForIndex,
      insertNewPanelForIndex,
      removePanelForIndex,
      setRhythmForIndex,
      setTitleForIndex,
      setWordsForIndex,
      rhythm,
    ],
  );

  const fetchPerformers = useCallback(async (filter: string) => {
    const { items } = await API.performers.getAll({
      filter,
      top: AUTOCOMPLETE_DEFAULT_ITEMS_SIZE,
    });
    return items;
  }, []);

  const fetchSongs = useCallback(async (filter: string) => {
    const { items } = await API.songs.getAll({
      filter,
      top: AUTOCOMPLETE_DEFAULT_ITEMS_SIZE,
    });
    return items;
  }, []);

  const onSongTitleSelect = (
    _e: SyntheticEvent<Element, Event>,
    _value: string | FetchedSong,
    reason: AutocompleteChangeReason,
  ) => {
    if (reason !== 'selectOption') return;
    const value = _value as FetchedSong;
    setSongTitle(value.songTitle);
    setPerformerBandName(value.performer.bandName);
  };

  const onPerformerSelect = (
    _e: SyntheticEvent<Element, Event>,
    _value: string | FetchedPerformer,
    reason: AutocompleteChangeReason,
  ) => {
    if (reason !== 'selectOption') return;
    const value = _value as FetchedPerformer;
    setPerformerBandName(value.bandName);
  };

  const [validationErrors, setValidationErrors] = useState<string[]>([]);

  useEffect(() => {
    const validate = async () => {
      try {
        await validationSchema.validate(
          {
            rhythm,
            songTitle,
            titles,
            wordPanels,
            performerBandName,
          },
          {
            abortEarly: false,
          },
        );
        setValidationErrors([]);
      } catch (errors) {
        const typedErrors = errors as { errors: string[] };
        setValidationErrors(typedErrors.errors);
      }
    };
    validate();
  }, [performerBandName, rhythm, songTitle, titles, wordPanels]);

  const renderSongOption = (
    props: React.HTMLAttributes<HTMLLIElement>,
    option: Song,
  ) => (
    <MenuItem {...props}>
      <span>
        <Typography>
          {option.songTitle} - {option.performer.bandName}
        </Typography>
      </span>
    </MenuItem>
  );

  const renderAutocompleteInput = ({
    label,
    ...inputProps
  }: OutlinedInputProps) =>
    function AutocompleteInput(params: AutocompleteRenderInputParams) {
      return (
        <FormControl sx={{ flexGrow: 1 }} fullWidth>
          <Typography component="label" gutterBottom variant="body2">
            {label}
          </Typography>
          <OutlinedInput
            id={params.id}
            {...params.InputProps}
            inputProps={params.inputProps}
            required
            {...inputProps}
          />
        </FormControl>
      );
    };

  return (
    <Box sx={{ width: '100%', mt: -6 }}>
      <CenterDesktopLayout ChildBoxProps={{ sx: { py: 4 } }}>
        <Breadcrumbs links={[links.mainPage, { label: 'Song Creation' }]} />
      </CenterDesktopLayout>
      <Divider />
      <CenterDesktopLayout>
        <Box
          component="main"
          sx={{
            pt: 10,
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            gap: 10,
          }}
        >
          <Box>
            <Typography variant="h4">General Info</Typography>
            <Box sx={{ display: 'flex', alignItems: 'center', gap: 6, mt: 5 }}>
              <Avatar sx={{ width: 80, height: 80 }} alt="Artist avatar">
                <CameraIcon />
              </Avatar>
              <Autocomplete
                getSuggestions={fetchPerformers}
                MuiAutocompleteProps={{
                  sx: { flexGrow: 1 },
                  inputValue: performerBandName,
                  onInputChange: (_, value) => setPerformerBandName(value),
                  onChange: onPerformerSelect,
                  getOptionLabel: props => (props as Performer).bandName,
                  freeSolo: true,
                  disableClearable: true,
                  renderInput: renderAutocompleteInput({
                    placeholder: 'Start typing Artist Name',
                    label: 'Artist Name *',
                  }),
                }}
              />
              <Autocomplete
                getSuggestions={fetchSongs}
                MuiAutocompleteProps={{
                  sx: { flexGrow: 1 },
                  inputValue: songTitle,
                  onInputChange: (_, value) => setSongTitle(value),
                  onChange: onSongTitleSelect,
                  getOptionLabel: props => (props as Song).songTitle,
                  freeSolo: true,
                  disableClearable: true,
                  renderOption: renderSongOption,
                  renderInput: renderAutocompleteInput({
                    placeholder: 'Start typing Song Name',
                    label: 'Song Name *',
                  }),
                }}
              />
            </Box>
          </Box>
          <Box>
            <Typography variant="h4" gutterBottom>
              Song Chords
            </Typography>
            <Box sx={{ display: 'flex', gap: 4, alignItems: 'center' }}>
              <Typography>Time signature:</Typography>
              <RhythmInput setRhythm={setRhythm} rhythm={rhythm} isEnabled />
            </Box>
          </Box>
          <DndProvider options={HTML5toTouch}>
            {rhythmPanels.map(generatePanel)}
          </DndProvider>
          <div>
            <GeneralValidation validationErrors={validationErrors} />
            <div>
              <LoadingButton
                loading={createSong.isPending || updateSong.isPending}
                disabled={
                  !!validationErrors.length ||
                  createSong.isPending ||
                  updateSong.isPending
                }
                onClick={onSubmitSong}
                variant="contained"
                size="large"
              >
                {song.data ? 'Edit chords' : 'Submit chords'}
              </LoadingButton>
            </div>
          </div>
        </Box>
      </CenterDesktopLayout>
    </Box>
  );
};
