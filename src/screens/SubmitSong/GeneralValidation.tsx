import { List, ListItem } from '@mui/material';
import { FC } from 'react';
import { useBoolean } from 'utils/hooks';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import { Error, ErrorOutline } from '@mui/icons-material';

export interface ErrorMessagesProps {
  validationErrors: string[];
}

export const GeneralValidation: FC<ErrorMessagesProps> = ({
  validationErrors,
}) => {
  const [isOpened, { toggle }] = useBoolean(false);

  return validationErrors.length ? (
    <List>
      <ListItemButton onClick={toggle}>
        <ListItemIcon>
          <Error color="error" />
        </ListItemIcon>
        <ListItemText primary="Gaps to fill" />
        {isOpened ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={isOpened} timeout="auto">
        <List>
          {validationErrors.map(error => (
            <ListItem key={error} disablePadding>
              <ListItemButton>
                <ListItemIcon>
                  <ErrorOutline />
                </ListItemIcon>
                <ListItemText primary={error} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </Collapse>
    </List>
  ) : null;
};
