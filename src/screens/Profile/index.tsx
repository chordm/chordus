'use client';

import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from '@mui/material';
import { useUserData, useLogOut } from 'queries/auth';
import ExitIcon from '@/public/icons/exit.svg';
import { CenterDesktopLayout } from 'layouts/DesktopLayout/CenterDesktopLayout';
import { useBoolean } from 'utils/hooks';

export const Profile = () => {
  const userData = useUserData();
  const logOut = useLogOut();
  const onLogOut = () => {
    off();
    logOut.mutate();
  };
  const [isDialogOpen, { on, off }] = useBoolean();
  return (
    <>
      <CenterDesktopLayout>
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Box sx={{ display: 'flex', columnGap: theme => theme.spacing(6) }}>
            <Avatar src={userData.data?.picture} sx={{ w: 8, h: 8, mr: 1 }}>
              {userData.data?.username?.[0].toUpperCase()}
            </Avatar>
            <Box>
              <Typography variant="bold">{userData.data?.username}</Typography>
              <Typography
                variant="body2"
                sx={{
                  color: theme => theme.palette.text.secondary,
                  fontWeight: 600,
                }}
              >
                {userData.data?.email}
              </Typography>
            </Box>
          </Box>
          <Button
            startIcon={<ExitIcon />}
            onClick={on}
            sx={{ px: 4, textTransform: 'capitalize' }}
          >
            <Typography variant="body2">log out</Typography>
          </Button>
        </Box>
        Liked Songs / Created Songs
      </CenterDesktopLayout>
      <Dialog
        open={isDialogOpen}
        onClose={off}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="xs"
      >
        <DialogTitle
          variant="h5"
          sx={{ fontWeight: 600 }}
          id="alert-dialog-title"
        >
          Confirm Logout
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            variant="body2"
            sx={{ p: 0 }}
            id="alert-dialog-description"
          >
            Are you sure you want to log out of your account?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={off} sx={{ mr: 2 }}>
            Back
          </Button>
          <Button
            disableElevation
            variant="contained"
            onClick={onLogOut}
            autoFocus
          >
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
