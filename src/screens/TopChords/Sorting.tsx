'use client';

import { FC, useContext } from 'react';
import { SortingOptions, sorting } from './types';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { GlobalStateContext } from 'providers/StateProvider';
import { useActor } from '@xstate/react';
import { useTopChordsSelectedQuery } from 'queries/common';

export const Sorting: FC = () => {
  const { isAnyLoading } = useTopChordsSelectedQuery();
  const globalServices = useContext(GlobalStateContext);
  const [topChordsState, sendTopChords] = useActor(globalServices.topChords);
  const { selectedTopVariant, selectedSortingOption } = topChordsState.context;

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth size="medium">
        <InputLabel id="display-mode-label">Sort by</InputLabel>
        <Select
          size="small"
          labelId="display-mode-label"
          id="display-mode"
          value={selectedSortingOption}
          label="Sort by"
          onChange={e => {
            sendTopChords({
              type: 'SET_SORTING_OPTION',
              sortingOption: e.target.value as SortingOptions,
            });
          }}
          disabled={isAnyLoading}
        >
          {sorting[selectedTopVariant].map(({ key, text }) => (
            <MenuItem key={key} value={key}>
              {text}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
};
