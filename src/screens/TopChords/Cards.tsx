'use client';

import { useActor } from '@xstate/react';
import { GlobalStateContext } from 'providers/StateProvider';
import { useRouter } from 'next/navigation';
import { Fragment, useCallback, useContext, useMemo } from 'react';
import { generatePath } from 'routing';
import { TopCard } from 'screens/TopChords/TopCard';
import { TopVariant } from 'screens/TopChords/types';
import { Performer, Song } from 'types';
import { useTopChordsSelectedQuery } from 'queries/common';

export const Cards = () => {
  const globalServices = useContext(GlobalStateContext);
  const [topChordsState] = useActor(globalServices.topChords);
  const router = useRouter();
  const { isAnyLoading, performersQuery, songsQuery } =
    useTopChordsSelectedQuery();

  const onItemClick = useCallback(
    ({ id }: Song | Performer) =>
      router.push(
        topChordsState.context.selectedTopVariant === TopVariant.chord
          ? generatePath.Chords(id as Song['id'])
          : generatePath.Performer(id as Performer['id']),
      ),

    [topChordsState.context.selectedTopVariant, router],
  );

  const songCards = useMemo(
    () =>
      songsQuery.data?.pages?.map((page, pageIndex) => (
        <Fragment key={pageIndex}>
          {page.items.map(song => (
            <TopCard
              key={'song' + song.id}
              onClick={() => onItemClick?.(song)}
              avatarImageUrl=""
              subheader={song.performer.bandName}
              rating={song.averageRating}
              title={song.songTitle}
              views={song.views}
              isLoading={isAnyLoading}
            />
          ))}
        </Fragment>
      )),
    [isAnyLoading, onItemClick, songsQuery.data?.pages],
  );

  const performerCards = useMemo(
    () =>
      performersQuery.data?.pages?.map((page, pageIndex) => (
        <Fragment key={pageIndex}>
          {page.items.map(performer => (
            <TopCard
              key={'performer' + performer.id}
              onClick={() => onItemClick?.(performer)}
              avatarImageUrl=""
              title={performer.bandName}
              rating={performer.averageRating ?? 0}
              subheader={performer.highlightedSong}
              // TODO: update types on FE and BE; on BE - update user
              views={performer.views}
            />
          ))}
        </Fragment>
      )),
    [onItemClick, performersQuery.data?.pages],
  );

  const renderCards = useMemo(() => {
    const currentCards =
      {
        [TopVariant.chord]: songCards,
        [TopVariant.performer]: performerCards,
      }[topChordsState.context.selectedTopVariant] ?? [];

    return currentCards;
  }, [songCards, performerCards, topChordsState.context.selectedTopVariant]);

  return renderCards;
};
