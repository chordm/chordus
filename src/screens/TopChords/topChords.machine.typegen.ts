// This file was automatically generated. Edits will be overwritten

export interface Typegen0 {
  '@@xstate/typegen': true;
  internalEvents: {
    'xstate.init': { type: 'xstate.init' };
  };
  invokeSrcNameMap: {};
  missingImplementations: {
    actions: never;
    delays: never;
    guards: never;
    services: never;
  };
  eventsCausingActions: {
    getSelectedSortingData: 'SET_SORTING_OPTION' | 'SET_TOP_VARIANT';
    setSortingOption: 'SET_SORTING_OPTION';
    setTopVariant: 'SET_TOP_VARIANT';
  };
  eventsCausingDelays: {};
  eventsCausingGuards: {};
  eventsCausingServices: {};
  matchesStates: undefined;
  tags: never;
}
