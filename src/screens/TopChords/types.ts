import { Performer, Song } from 'types';

export enum TopVariant {
  chord = 'CHORD',
  performer = 'PERFORMER',
}

export enum SortingOptions {
  rating = 'RATING',
  views = 'VIEWS',
}

export type Sorting = {
  [Key in TopVariant]: {
    key: SortingOptions;
    property: Key extends TopVariant.chord
      ? keyof Song
      : Key extends TopVariant.performer
        ? keyof Performer
        : never;
    text: string;
  }[];
};

export const sorting = {
  [TopVariant.chord]: [
    { key: SortingOptions.views, property: 'views', text: 'Views' },
    { key: SortingOptions.rating, property: 'averageRating', text: 'Rating' },
  ],
  [TopVariant.performer]: [
    { key: SortingOptions.views, property: 'views', text: 'Views' },
    { key: SortingOptions.rating, property: 'averageRating', text: 'Rating' },
  ],
} satisfies Sorting;

export const defaultSorting = {
  [TopVariant.chord]: SortingOptions.views,
  [TopVariant.performer]: SortingOptions.views,
} as const;

export const defaultTopVariant = TopVariant.chord;

export const getSortingData = (
  topVariant: TopVariant,
  sortingOption: SortingOptions,
) => sorting[topVariant].find(({ key }) => key === sortingOption)!;
