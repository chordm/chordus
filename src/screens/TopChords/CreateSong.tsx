import { Box, Fab, Typography } from '@mui/material';
import PlusIcon from '@/public/icons/plus.svg';
import Link from 'next/link';

export const CreateSong = () => (
  <Box
    sx={{
      position: 'fixed',
      bottom: 40,
      display: 'grid',
      gridTemplateColumns: 'auto minmax(auto, 900px) auto',
    }}
  >
    <Box sx={{ gridColumn: 2 }}>
      <Fab
        href="/submit-song"
        LinkComponent={Link}
        sx={{
          textTransform: 'none',
          ml: 'auto',
          display: 'flex',
          gap: 3,
          p: 4,
          alignItems: 'center',
          g: { stroke: 'white' },
          height: 'unset',
          borderRadius: 4,
          width: 'fit-content',
        }}
        variant="extended"
        size="large"
        color="primary"
      >
        <PlusIcon />
        <Typography variant="body2" color="white">
          Create Song
        </Typography>
      </Fab>
    </Box>
  </Box>
);
