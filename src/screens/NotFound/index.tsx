import { FC } from 'react';
import styles from './index.module.scss';
// import { useRouter } from 'next/router';
// import { useTranslation } from 'next-i18next';
import { ResponsiveContainer } from 'layouts/ResponsiveContainer';

export const NotFound: FC = () => {
  // const router = useRouter();
  // const { t } = useTranslation('common');

  // const goBack = useCallback(() => router.back(), [router]);

  return (
    <ResponsiveContainer>
      <main className={styles.root}>
        {/* <Card>
          <Card.Content className={styles.content}>
            <Card.Header>{t('not-found.title')}</Card.Header>
            <Card.Meta>The page you are looking for is not found</Card.Meta>
          </Card.Content>
          <Card.Content extra>
            <Button secondary floated="right" onClick={goBack}>
              Go back
            </Button>
          </Card.Content>
        </Card> */}
        404
      </main>
    </ResponsiveContainer>
  );
};
