import { HydrationBoundary, dehydrate } from '@tanstack/react-query';
import { usePrefetchSong } from 'queries/songs';
import { Chords } from 'screens/Chords';
import getQueryClient from 'utils/getQueryClient';

export default async function Song(props: { params: { songId: string } }) {
  const queryClient = getQueryClient();
  const songId = parseInt(String(props.params.songId), 10);

  await usePrefetchSong(songId);

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <Chords />
    </HydrationBoundary>
  );
}
