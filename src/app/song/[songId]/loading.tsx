import { Box, Divider, Skeleton } from '@mui/material';
import { Breadcrumbs, links } from 'components/Breadcrumbs';
import { Rating } from 'components/Rating';
import { CenterDesktopLayout } from 'layouts/DesktopLayout/CenterDesktopLayout';
import { Actions } from 'screens/Chords/Actions';
import { AdjustButtons } from 'screens/Chords/AdjustButtons';

export default function Loading() {
  return (
    <Box sx={{ width: '100%', mt: -6 }}>
      <CenterDesktopLayout
        ChildBoxProps={{
          sx: {
            py: 4,
          },
        }}
      >
        <Breadcrumbs links={[links.mainPage, { showSkeleton: true }]} />
      </CenterDesktopLayout>
      <Divider />
      <CenterDesktopLayout>
        <Box
          component="main"
          sx={{ display: 'flex', flexDirection: 'column', gap: 6, pt: 6 }}
        >
          <Skeleton width={600} sx={{ height: 40 }} />
          <Box sx={{ display: 'flex', alignItems: 'center', gap: 9 }}>
            {Array.from({ length: 3 }).map((_, index) => (
              <Skeleton
                key={index}
                variant="rounded"
                sx={{ width: 190, height: 36 }}
              />
            ))}
          </Box>
          <Skeleton variant="rounded" width={120} sx={{ height: 24 }} />
          <Box sx={{ display: 'flex', alignItems: 'center', gap: 12 }}>
            {Array.from({ length: 3 }).map((_, index) => (
              <Skeleton
                key={index}
                variant="rounded"
                sx={{ width: 190, height: 68 }}
              />
            ))}
          </Box>
          <Skeleton variant="rounded" sx={{ height: 120, width: 300 }} />
          <Skeleton variant="rounded" sx={{ height: 120, width: 500 }} />
          <Skeleton variant="rounded" sx={{ height: 120, width: 400 }} />
          <Box>
            <Skeleton variant="text" sx={{ width: 80 }} />
            <Skeleton variant="rounded" sx={{ width: 140, height: 30 }} />
          </Box>
        </Box>
      </CenterDesktopLayout>
    </Box>
  );
}
