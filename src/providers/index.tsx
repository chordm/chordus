'use client';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { PropsWithChildren, useState } from 'react';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { MUIProvider } from 'providers/MUIProvider';
import { GlobalStateProvider } from 'providers/StateProvider';

export default function Providers({ children }: PropsWithChildren) {
  const [queryClient] = useState(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            retry: false,
            refetchOnWindowFocus: false,
          },
        },
      }),
  );

  return (
    <QueryClientProvider client={queryClient}>
      <MUIProvider>
        <GlobalStateProvider>{children}</GlobalStateProvider>
      </MUIProvider>
      <ReactQueryDevtools initialIsOpen />
    </QueryClientProvider>
  );
}
