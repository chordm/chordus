import { Button, ButtonProps, styled } from '@mui/material';

const sizeToJSS = {
  large: {
    '&.MuiButtonBase-root': {
      padding: 8,
      borderRadius: 8,
    },
  },
  small: {
    '&.MuiButtonBase-root': {
      padding: 0,
      borderRadius: 4,
    },
  },
} satisfies Record<NonNullable<ButtonProps['size']>, any>;

export const OutlinedIconButton = styled(Button)(({ size = 'large' }) => ({
  '&.MuiButtonBase-root': {
    ...sizeToJSS[size!]?.['&.MuiButtonBase-root'],
    minWidth: 'unset',
  },
}));
