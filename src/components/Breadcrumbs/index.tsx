import { Skeleton, Typography } from '@mui/material';
import MUIBreadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import { FC } from 'react';

export interface BreadcrumbLink {
  url: string;
  label: string;
  showSkeleton?: boolean;
}

interface BreadcrumbsProps {
  links: [...BreadcrumbLink[], Partial<BreadcrumbLink>];
}

export const links = {
  mainPage: {
    label: 'Main Page',
    url: '/',
  },
} satisfies Record<string, BreadcrumbLink>;

export const Breadcrumbs: FC<BreadcrumbsProps> = ({ links }) => {
  return (
    <MUIBreadcrumbs aria-label="breadcrumb">
      {links.map((link, index) =>
        link.showSkeleton ? (
          <Skeleton width={200} key={index} variant="text" />
        ) : (link as BreadcrumbLink).url ? (
          <Link
            href={(link as BreadcrumbLink).url}
            underline="always"
            color="inherit"
            variant="body2"
            key={index}
          >
            {link.label}
          </Link>
        ) : (
          <Typography variant="body2" color="inherit" key={index}>
            {link.label}
          </Typography>
        ),
      )}
    </MUIBreadcrumbs>
  );
};
