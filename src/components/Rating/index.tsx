'use client';

import React, { FC } from 'react';
import MUIRating, { RatingProps as MUIRatingProps } from '@mui/material/Rating';
import StarIcon from '@/public/icons/star.svg';
import { colors } from 'utils/theme';
import { styled } from '@mui/material/styles';

export interface RatingProps extends MUIRatingProps {
  rating?: number;
  setRating?: (rating: number) => void;
  isLoading?: boolean;
}

const StyledRating = styled(MUIRating)(({ theme }) => ({
  '&.MuiRating-root': {
    columnGap: theme.spacing(1),
  },
  '& .MuiRating-iconHover': {
    transform: 'scale(1.1)',
  },
})) as typeof MUIRating;

export const Rating: FC<RatingProps> = ({
  setRating,
  rating,
  isLoading = false,
  ...rest
}) => {
  const onRate = (
    _e: React.SyntheticEvent<Element, Event>,
    value: number | null,
  ) => {
    setRating?.(Number(value));
  };

  return (
    <StyledRating
      name="simple-controlled"
      value={rating}
      emptyIcon={<StarIcon style={{ fill: 'none' }} />}
      icon={<StarIcon style={{ fill: colors.black }} />}
      onChange={onRate}
      disabled={isLoading}
      {...rest}
    />
  );
};
