import type { Meta, StoryObj } from '@storybook/react';
import { Rating } from '.';
import { useState } from 'react';

const meta: Meta<typeof Rating> = {
  component: Rating,
};

export default meta;

type Story = StoryObj<typeof Rating>;

const defaultRating = 3;

const InteractiveRating = () => {
  const [value, setValue] = useState(defaultRating);

  return <Rating value={value} setRating={setValue} />;
};

export const Default: Story = {
  args: {
    rating: defaultRating,
    isLoading: false,
  },
};

export const Interactive: Story = {
  render: () => <InteractiveRating />,
};

export const Loading: Story = {
  args: {
    rating: defaultRating,
    isLoading: true,
  },
};
