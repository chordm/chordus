import { FC } from 'react';
import { Autocomplete, TextField, Typography } from '@mui/material';

const sectionTypes = [
  { label: 'Intro', id: 'Intro' },
  { label: 'Verse', id: 'Verse' },
  { label: 'Chorus', id: 'Chorus' },
] as const;

export const Title: FC<{
  onSubmit?: (text: string) => void;
  title?: string;
  isEnabled?: boolean;
}> = ({ onSubmit, title = '', isEnabled = true }) => {
  return isEnabled ? (
    <Typography component="label" variant="body2">
      Section Type
      <Autocomplete
        sx={{ mb: 8, mt: 1 }}
        disableClearable
        defaultValue={sectionTypes[1]}
        disablePortal
        id="chord-section-title"
        options={sectionTypes}
        freeSolo
        onChange={(_event, value) => {
          if (value === null) return;
          const stringifiedValue =
            typeof value === 'string'
              ? value
              : typeof value === 'object'
                ? value.id
                : '';
          onSubmit?.(stringifiedValue);
        }}
        renderInput={params => (
          <TextField {...params} placeholder="Select Section Type" />
        )}
      />
    </Typography>
  ) : title ? (
    <span>{title}</span>
  ) : null;
};
