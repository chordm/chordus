'use client';

import { Dispatch, FC, SetStateAction, useCallback, useEffect } from 'react';
import {
  Chord,
  ChordAlign,
  ChordItem,
  ChordPanel as ChordPanelType,
  RhythmPanel,
} from 'types';
import { ChordLine } from './ChordLine';
import styles from './index.module.scss';
import { produce } from 'immer';
import { generateRhythm, getRawChordIndex } from 'utils/chords';
import { v4 as uuidv4 } from 'uuid';
import { Title } from './Title';
import { ChordManager, ManagementItems } from 'components/ChordManager';
import { Box, Paper } from '@mui/material';

export interface ChordPanelProps {
  rhythm: [number, number];
  rhythmLines: RhythmPanel;
  wordLines: ChordPanelType;
  title: string;
  isEditing?: boolean;
  setWordLines?: Dispatch<SetStateAction<ChordPanelType>>;
  setRhythmLines?: Dispatch<SetStateAction<RhythmPanel>>;
  setTitle?: Dispatch<SetStateAction<string>>;
  onDelete?: () => void;
  onInsertNew?: () => void;
  disablePanelRemoving?: boolean;
}

export const ChordPanel: FC<ChordPanelProps> = ({
  rhythm,
  isEditing = false,
  title,
  setTitle,
  rhythmLines,
  setRhythmLines,
  setWordLines,
  wordLines,
  onDelete,
  onInsertNew,
  disablePanelRemoving,
}) => {
  useEffect(() => {
    if (
      rhythmLines[rhythmLines.length - 1].length - 1 !== rhythm[0] &&
      !wordLines[wordLines.length - 1].length
    ) {
      setRhythmLines?.([
        ...rhythmLines.slice(0, rhythmLines.length - 1),
        [generateRhythm(rhythm[0])],
      ]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rhythm]);

  useEffect(() => {
    if (!rhythmLines.length) {
      setRhythmLines?.([[generateRhythm(rhythm[0])]]);
      setWordLines?.([[]]);
    }
  }, [rhythm, rhythmLines.length, setRhythmLines, setWordLines]);

  const setChordForWord = useCallback(
    (lineIndex: number) =>
      (wordIndex: number, chordAlign: Omit<ChordAlign, 'lineIndex'>) =>
        setWordLines?.(
          produce((wordLines: ChordPanelType) => {
            wordLines[lineIndex].forEach((mappedWord, mappedWordIndex) => {
              const rawMappedWordIndex = getRawChordIndex(
                mappedWord.chordAlign.tactIndex,
                mappedWord.chordAlign.chordIndex,
                rhythmLines[lineIndex],
              );
              const rawWordIndex = getRawChordIndex(
                chordAlign.tactIndex,
                chordAlign.chordIndex,
                rhythmLines[lineIndex],
              );

              const isMoveAdjacentWords =
                (mappedWordIndex < wordIndex &&
                  rawMappedWordIndex >= rawWordIndex) ||
                (mappedWordIndex > wordIndex &&
                  rawMappedWordIndex <= rawWordIndex) ||
                mappedWordIndex === wordIndex;

              mappedWord.chordAlign = {
                lineIndex,
                chordIndex: isMoveAdjacentWords
                  ? chordAlign.chordIndex
                  : mappedWord.chordAlign.chordIndex,
                tactIndex: isMoveAdjacentWords
                  ? chordAlign.tactIndex
                  : mappedWord.chordAlign.tactIndex,
              };
            });
          }),
        ),
    [rhythmLines, setWordLines],
  );

  const setChord = useCallback(
    (lineIndex: number) =>
      (
        chord: Chord | null,
        chordAlign: Omit<ChordAlign, 'lineIndex'>,
        wordIndex?: number,
      ) => {
        setRhythmLines?.(
          produce(chords => {
            chords[lineIndex][chordAlign.tactIndex][chordAlign.chordIndex] =
              chord;
          }),
        );

        if (wordIndex) {
          setWordLines?.(
            produce(words => {
              words[lineIndex][wordIndex].chordAlign = {
                ...chordAlign,
                lineIndex,
              };
            }),
          );
        }
      },
    [setRhythmLines, setWordLines],
  );

  const appendTact = useCallback(
    (lineIndex: number) => () => {
      setRhythmLines?.(
        produce(rhythmLine => {
          rhythmLine[lineIndex].push(Array(rhythm[0]).fill(null));
        }),
      );
    },
    [rhythm, setRhythmLines],
  );

  const removeLastTact = useCallback(
    (lineIndex: number) => () => {
      setWordLines?.(
        produce((wordLines: ChordPanelType) => {
          if (rhythmLines[lineIndex].length === 1) {
            return [
              ...wordLines.slice(0, lineIndex),
              ...wordLines.slice(lineIndex + 1),
            ];
          } else {
            for (let i = wordLines[lineIndex].length - 1; i > 0; i--) {
              const word = wordLines[lineIndex][i];
              if (
                word.chordAlign.tactIndex ===
                rhythmLines[lineIndex].length - 1
              ) {
                word.chordAlign.tactIndex--;
                word.chordAlign.chordIndex =
                  rhythmLines[lineIndex][word.chordAlign.tactIndex].length - 1;
              }
            }
          }
        }),
      );

      setRhythmLines?.(
        produce((rhythmLines: RhythmPanel) => {
          if (rhythmLines[lineIndex].length === 1) {
            return [
              ...rhythmLines.slice(0, lineIndex),
              ...rhythmLines.slice(lineIndex + 1),
            ];
          } else {
            rhythmLines[lineIndex].pop();
          }
        }),
      );
    },
    [rhythmLines, setRhythmLines, setWordLines],
  );

  const insertChordLine = useCallback(
    (lineIndex: number) => () => {
      setRhythmLines?.(rhythmLine => [
        ...rhythmLine.slice(0, lineIndex + 1),
        [generateRhythm(rhythm[0])],
        ...rhythmLine.slice(lineIndex + 1),
      ]);
      setWordLines?.(wordLines => [
        ...wordLines.slice(0, lineIndex + 1),
        [],
        ...wordLines.slice(lineIndex + 1),
      ]);
    },
    [rhythm, setRhythmLines, setWordLines],
  );

  const removeChordLine = useCallback(
    (lineIndex: number) => () => {
      setRhythmLines?.(rhythmLines => [
        ...rhythmLines.slice(0, lineIndex),
        ...rhythmLines.slice(lineIndex + 1),
      ]);
      setWordLines?.(wordLines => [
        ...wordLines.slice(0, lineIndex),
        ...wordLines.slice(lineIndex + 1),
      ]);
    },
    [setRhythmLines, setWordLines],
  );

  const changeChordsWords = useCallback(
    (lineIndex: number) =>
      (tactIndex: number, chordIndex: number, rawWords: string) => {
        const wordsList = rawWords.split(/\s+/).filter(Boolean);
        const chordItemsFromWords: ChordItem[] = wordsList.map(word => ({
          word,
          chordAlign: { lineIndex, tactIndex, chordIndex },
          id: uuidv4(),
        }));
        const rawChordIndex = getRawChordIndex(
          tactIndex,
          chordIndex,
          rhythmLines[lineIndex],
        );
        setWordLines?.(
          produce((wordLines: ChordPanelType) => {
            const filteredOldWords = wordLines[lineIndex].filter(
              ({ chordAlign }) =>
                chordAlign.tactIndex !== tactIndex ||
                chordAlign.chordIndex !== chordIndex,
            );
            let chordIndexToInsertWords = 0;

            for (let i = 0; i < filteredOldWords.length; i++) {
              const existingRawIndex = getRawChordIndex(
                filteredOldWords[i].chordAlign.tactIndex,
                filteredOldWords[i].chordAlign.chordIndex,
                rhythmLines[lineIndex],
              );
              if (rawChordIndex > existingRawIndex) {
                chordIndexToInsertWords = i;
              } else {
                break;
              }
            }

            wordLines[lineIndex] = [
              ...filteredOldWords.slice(0, chordIndexToInsertWords + 1),
              ...chordItemsFromWords,
              ...filteredOldWords.slice(
                chordIndexToInsertWords + 1,
                filteredOldWords.length,
              ),
            ];
          }),
        );
      },
    [rhythmLines, setWordLines],
  );

  const copyLineToTheEnd = useCallback(
    (lineIndex: number) => () => {
      setWordLines?.(words => [...words, []]);
      setRhythmLines?.(lines => [...lines, lines[lineIndex]]);
    },
    [setRhythmLines, setWordLines],
  );

  return (
    <section className={styles.section}>
      <Paper variant="outlined" className={styles.root}>
        <Box className={styles.chordPanel}>
          <Title onSubmit={setTitle} title={title} isEnabled={isEditing} />
          {wordLines.map((wordLine, lineIndex) => (
            <ChordLine
              key={lineIndex}
              isEditing={isEditing}
              rhythmLine={rhythmLines[lineIndex]}
              wordLine={wordLine}
              lineIndex={lineIndex}
              setChordForWord={setChordForWord(lineIndex)}
              rhythm={rhythm}
              setChord={setChord(lineIndex)}
              appendTact={appendTact(lineIndex)}
              removeLastTact={removeLastTact(lineIndex)}
              insertChordLine={insertChordLine(lineIndex)}
              removeChordLine={removeChordLine(lineIndex)}
              changeChordsWords={changeChordsWords(lineIndex)}
              copyLineToTheEnd={copyLineToTheEnd(lineIndex)}
            />
          ))}
        </Box>
      </Paper>
      {isEditing && (
        <ChordManager
          onDelete={onDelete}
          onInsertNew={onInsertNew}
          itemsOptions={{
            [ManagementItems.ADD]: {},
            [ManagementItems.REMOVE]: {
              disabled: disablePanelRemoving,
            },
          }}
        />
      )}
    </section>
  );
};
