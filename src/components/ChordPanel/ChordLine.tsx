'use client';

import { FC, useCallback, useMemo, useRef, useState } from 'react';
import { ChordAlign, ChordItem, ChordString, RhythmLine, Chord } from 'types';
import styles from './index.module.scss';
import commonStyles from 'styles/common.module.scss';
import { useOnClickOutside } from 'utils/hooks';
import {
  createRhythmId,
  getRawChordIndex,
  parseChordFromString,
  stringifyChord,
} from 'utils/chords';
import { ChordForm } from './ChordForm';
import { RhythmPoint } from './RhythmPoint';
import { Word } from './Word';
import { WordsGridItem } from './WordsGridItem';
import { WordsInput } from './WordsInput';
import { clsx } from 'clsx';
import Image from 'next/image';
import MinusIcon from '@/public/icons/minus.svg';
import PlusIcon from '@/public/icons/plus.svg';
import CopyIcon from '@/public/icons/copy.svg';
import { OutlinedIconButton } from 'components/OutlinedIconButton';
import { Box, Tooltip, useTheme } from '@mui/material';

export interface RhythmLineProps {
  rhythmLine: RhythmLine;
  wordLine: ChordString;
  lineIndex: number;
  setChordForWord: (wordIndex: number, chordAlign: ChordAlign) => void;
  rhythm: [number, number];
  setChord: (
    chord: Chord | null,
    chordAlign: Omit<ChordAlign, 'lineIndex'>,
    wordIndex?: number,
  ) => void;
  appendTact: () => void;
  removeLastTact: () => void;
  insertChordLine: () => void;
  removeChordLine: () => void;
  copyLineToTheEnd: () => void;
  isEditing: boolean;
  changeChordsWords: (
    tactIndex: number,
    chordIndex: number,
    rawWords: string,
  ) => void;
}

export const ChordLine: FC<RhythmLineProps> = ({
  rhythmLine,
  wordLine,
  lineIndex,
  setChordForWord,
  setChord,
  appendTact,
  removeLastTact,
  insertChordLine,
  removeChordLine,
  changeChordsWords,
  copyLineToTheEnd,
  isEditing,
}) => {
  const theme = useTheme();

  const [chordFormAlign, setChordFormAlign] = useState<Omit<
    ChordAlign,
    'lineIndex'
  > | null>(null);
  const [editingPlaceAlign, setEditingPlaceAlign] = useState<Omit<
    ChordAlign,
    'lineIndex'
  > | null>(null);

  const rootRef = useRef<HTMLDivElement>(null);
  const hideForm = useCallback(() => setChordFormAlign(null), []);
  const chordFormTextInput = useRef<HTMLSpanElement>(null);
  const [chordFormWordIndex, setChordFormWordIndex] = useState<number>();

  const showTextEditing = useCallback(
    (tactIndex: number, chordIndex: number) => () =>
      setEditingPlaceAlign({ tactIndex, chordIndex }),
    [],
  );

  const submitChordForm = useCallback(() => {
    if (!chordFormTextInput.current || !chordFormAlign) return;
    const chordsText = chordFormTextInput.current?.innerText;
    const chord = parseChordFromString(chordFormTextInput.current?.innerText);
    if (chord !== null || chordsText === '') {
      setChord(chord, chordFormAlign, chordFormWordIndex);
    }
    hideForm();
    setChordFormWordIndex(undefined);
  }, [chordFormAlign, chordFormWordIndex, hideForm, setChord]);

  const onRhythmPointClick = useCallback(
    (align: typeof chordFormAlign) => {
      setChordFormWordIndex(undefined);
      return () => setChordFormAlign(align);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [chordFormAlign],
  );

  useOnClickOutside(chordFormTextInput, submitChordForm);

  const rawRhythmLine = useMemo(() => rhythmLine.flat(), [rhythmLine]);

  const editLineActions = useMemo(
    () => (
      <Box
        sx={{
          ml: 8,
          height: 0,
          width: 64,
          display: 'flex',
          gap: 4,
          transform: 'translateY(-12px)',
        }}
      >
        <Tooltip title="Remove last bar" PopperProps={{ placement: 'top' }}>
          <OutlinedIconButton
            onClick={removeLastTact}
            variant="outlined"
            size="small"
            sx={{
              background: 'white',
              height: 'max-content',
              '&.MuiButtonBase-root:hover': {
                background: 'white',
              },
            }}
          >
            <MinusIcon className={styles.tactIcon} />
          </OutlinedIconButton>
        </Tooltip>
        <Tooltip title="Append bars" PopperProps={{ placement: 'top' }}>
          <OutlinedIconButton
            onClick={appendTact}
            variant="outlined"
            size="small"
            sx={{
              background: 'white',
              height: 'max-content',
              '&.MuiButtonBase-root:hover': {
                background: 'white',
              },
            }}
          >
            <PlusIcon className={styles.tactIcon} />
          </OutlinedIconButton>
        </Tooltip>
      </Box>
    ),
    [appendTact, removeLastTact],
  );

  const addChordLineButton = useMemo(
    () => (
      <Box
        sx={{
          gridColumn: rawRhythmLine.length + 2,
          display: 'flex',
          gap: 2,
          gridRow: 2,
          minWidth: 'max-content !important',
          alignItems: 'flex-start',
          ml: 4,
          mt: -3,
        }}
      >
        <Tooltip title="Remove lyrics row">
          <OutlinedIconButton variant="outlined" onClick={removeChordLine}>
            <MinusIcon />
          </OutlinedIconButton>
        </Tooltip>
        <Tooltip title="Add lyrics row">
          <OutlinedIconButton variant="outlined" onClick={insertChordLine}>
            <PlusIcon />
          </OutlinedIconButton>
        </Tooltip>
        <Tooltip title="Copy current lyrics row">
          <OutlinedIconButton variant="outlined" onClick={copyLineToTheEnd}>
            <CopyIcon />
          </OutlinedIconButton>
        </Tooltip>
      </Box>
    ),
    [rawRhythmLine.length, removeChordLine, insertChordLine, copyLineToTheEnd],
  );

  const onWordsInputSubmit = useCallback(
    (breakOnVoidText: boolean, tactIndex: number, chordIndex: number) =>
      (text: string) => {
        if (breakOnVoidText && !text) return;
        changeChordsWords(tactIndex, chordIndex, text);
        setEditingPlaceAlign(null);
      },
    [changeChordsWords],
  );

  const wordsGrid = useMemo(() => {
    const grid: (ChordItem & { wordIndex: number })[][] = Array.from(
      { length: rawRhythmLine.length },
      () => [],
    );
    let lastRawChordIndex = 0;

    wordLine.forEach((word, wordIndex) => {
      if (word.chordAlign) {
        lastRawChordIndex = getRawChordIndex(
          word.chordAlign.tactIndex,
          word.chordAlign.chordIndex,
          rhythmLine,
        );
      }
      grid[lastRawChordIndex].push({ ...word, wordIndex });
    });

    return grid;
  }, [rawRhythmLine.length, wordLine, rhythmLine]);

  const words = useMemo(
    () =>
      rhythmLine.map((rhythmTact, tactIndex) =>
        rhythmTact.map((rhythmChord, chordIndex) => {
          const rawChordIndex = getRawChordIndex(
            tactIndex,
            chordIndex,
            rhythmLine,
          );
          const rawText = wordsGrid[rawChordIndex]
            ?.map(({ word }) => word)
            .join(' ');

          return editingPlaceAlign?.chordIndex === chordIndex &&
            editingPlaceAlign?.tactIndex === tactIndex ? (
            <WordsInput
              spanFrom={rawChordIndex + 1}
              spanUntil={rawChordIndex + 2}
              onSubmit={onWordsInputSubmit(false, tactIndex, chordIndex)}
              initialText={rawText}
              isLastColumn={rawChordIndex === rawRhythmLine.length - 1}
              isFirstColumn={rawChordIndex === 0}
              isEnabled={isEditing}
            />
          ) : (
            <WordsGridItem
              key={rawChordIndex}
              chordIndex={chordIndex}
              lineIndex={lineIndex}
              tactIndex={tactIndex}
              setChordForWord={setChordForWord}
              setChordFormAlign={setChordFormAlign}
              showSetChordForm={!rhythmChord}
              setChordFormWordIndex={setChordFormWordIndex}
              isLast={rawChordIndex === rawRhythmLine.length - 1}
              onActive={showTextEditing(tactIndex, chordIndex)}
              isEnabled={isEditing}
            >
              {wordsGrid[rawChordIndex]?.map(({ word, wordIndex, id }, i) => (
                <Word
                  isFirst={i === 0 && rawChordIndex !== 0}
                  wordAlign={{ lineIndex, wordIndex }}
                  text={word}
                  key={id}
                />
              ))}
            </WordsGridItem>
          );
        }),
      ),
    [
      editingPlaceAlign,
      isEditing,
      lineIndex,
      onWordsInputSubmit,
      rawRhythmLine.length,
      rhythmLine,
      setChordForWord,
      showTextEditing,
      wordsGrid,
    ],
  );

  const rhythmPoints = useMemo(
    () =>
      rhythmLine.map((rhythmTact, tactIndex) =>
        rhythmTact.map((chord, chordIndex) => {
          if (tactIndex === 0 && chordIndex === 0) return null;
          const editChord = onRhythmPointClick({ chordIndex, tactIndex });
          const id = createRhythmId(tactIndex, chordIndex);
          const rawChordIndex = getRawChordIndex(
            tactIndex,
            chordIndex,
            rhythmLine,
          );
          const isShowChordForm =
            chordFormAlign?.tactIndex === tactIndex &&
            chordFormAlign.chordIndex === chordIndex;
          const absoluteChordIndex = rawChordIndex + lineIndex * 100;
          const rhythmPointElement = (
            <RhythmPoint
              isWeak={
                (tactIndex === 0 && chordIndex !== 1) ||
                (tactIndex !== 0 && chordIndex !== 0)
              }
              isChordSet={!!chord}
              onClick={editChord}
              showLine={rawChordIndex !== rawRhythmLine.length - 1}
              tabIndex={absoluteChordIndex}
              isEnabled={isEditing}
            />
          );

          return (
            <div
              key={id}
              id={id}
              className={styles.rhythmPointWrapper}
              style={{
                gridColumnStart:
                  tactIndex === 0 && chordIndex === 1 ? 2 : undefined,
              }}
            >
              {(chord !== null || isShowChordForm) && (
                <div
                  key={'rhythmPoint' + id}
                  className={clsx(styles.chordWrapper, {
                    [commonStyles.disabled]: !isEditing,
                  })}
                >
                  <span
                    className={styles.chord}
                    onClick={editChord}
                    role="button"
                    aria-hidden="true"
                    style={{ ...theme.typography.body2 }}
                  >
                    {isShowChordForm && chordFormAlign ? (
                      <ChordForm
                        chordAlign={chordFormAlign}
                        onSubmit={submitChordForm}
                        textInput={chordFormTextInput}
                        defaultValue={chord ? stringifyChord(chord) : undefined}
                        tabIndex={absoluteChordIndex}
                        isEnabled={isEditing}
                      />
                    ) : chord !== null ? (
                      stringifyChord(chord)
                    ) : null}
                  </span>
                </div>
              )}
              {rhythmLine.length - 1 === tactIndex &&
              rhythmTact.length - 1 === chordIndex &&
              isEditing ? (
                <>
                  {rhythmPointElement}
                  {editLineActions}
                </>
              ) : (
                rhythmPointElement
              )}
            </div>
          );
        }),
      ),
    [
      rhythmLine,
      onRhythmPointClick,
      chordFormAlign,
      lineIndex,
      submitChordForm,
      rawRhythmLine.length,
      isEditing,
    ],
  );

  return (
    <div
      className={styles.rhythmLine}
      style={{
        gridTemplateColumns: `repeat(${
          rawRhythmLine.length + 1
        }, min-content) 1fr`,
      }}
      ref={rootRef}
    >
      {rhythmPoints}
      {wordLine.length ? (
        words
      ) : (
        <>
          <div
            className={clsx(
              styles.inputWordsPrefix,
              styles.wordsGridItem,
              styles.firstWordsOnLine,
            )}
            aria-hidden="true"
          />
          <WordsInput
            spanFrom={2}
            spanUntil={rawRhythmLine.length + 1}
            onSubmit={onWordsInputSubmit(true, 0, 1)}
            placeholder="Lyrics ..."
            isLastColumn
            isEnabled
          />
        </>
      )}
      {isEditing && addChordLineButton}
    </div>
  );
};
