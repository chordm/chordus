import { clsx } from 'clsx';
import { FC } from 'react';
import styles from './index.module.scss';
import commonStyles from 'styles/common.module.scss';

export const RhythmPoint: FC<{
  isWeak: boolean;
  isChordSet: boolean;
  onClick: () => void;
  showLine: boolean;
  tabIndex: number;
  isEnabled: boolean;
}> = ({
  isWeak = false,
  isChordSet,
  onClick,
  showLine,
  tabIndex,
  isEnabled = true,
}) => (
  <>
    {showLine && <div className={styles.rhythmPointLine} />}
    <button
      tabIndex={tabIndex}
      onSelect={isEnabled ? onClick : undefined}
      onClick={isEnabled ? onClick : undefined}
      className={clsx(styles.rhythmPoint, {
        [styles.strong]: !isWeak,
        [styles.weak]: isWeak,
        [styles.isChordSet]: isChordSet,
        [commonStyles.disabled]: !isEnabled,
      })}
    />
  </>
);
