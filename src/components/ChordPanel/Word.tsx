import { clsx } from 'clsx';
import { FC } from 'react';
import styles from './index.module.scss';
import { DndTypes } from '../../constant';
import { DragSourceMonitor, useDrag } from 'react-dnd';
import { DragObject } from 'components/ChordPanel/types';

export const Word: FC<{
  text?: string;
  wordAlign: { lineIndex: number; wordIndex: number };
  isFirst: boolean;
}> = ({ text, wordAlign, isFirst }) => {
  const [{ isDragging }, drag] = useDrag<DragObject, any, any>({
    type: DndTypes.CHORD_ELEMENT,
    item: { type: DndTypes.CHORD_ELEMENT, wordAlign },
    collect: (monitor: DragSourceMonitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  return (
    <span
      ref={drag}
      className={clsx(styles.word, {
        [styles.invisible]: isDragging,
        [styles.firstWord]: isFirst,
      })}
    >
      {text}
    </span>
  );
};
