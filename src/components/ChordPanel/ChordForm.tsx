import React, { FC, RefObject, useCallback, useLayoutEffect } from 'react';
import { ChordAlign } from 'types';
import { selectAllTextInElement } from 'utils/misc';
import styles from './index.module.scss';
import { useTheme } from '@mui/material';

export const ChordForm: FC<{
  chordAlign: Omit<ChordAlign, 'lineIndex'>;
  onSubmit: () => void;
  textInput: RefObject<HTMLSpanElement>;
  defaultValue?: string;
  tabIndex: number;
  isEnabled: boolean;
}> = ({ onSubmit, textInput, tabIndex, defaultValue = '', isEnabled }) => {
  const theme = useTheme();

  const onKeyDown = useCallback(
    (e: React.KeyboardEvent<HTMLSpanElement>) => {
      if (e.key === 'Enter') {
        e.preventDefault();
        textInput.current?.blur();
      }
    },
    [textInput],
  );

  useLayoutEffect(() => {
    if (textInput.current) {
      textInput.current.focus();
      textInput.current.innerText = defaultValue;
      selectAllTextInElement(textInput.current);
    }
  }, [defaultValue, isEnabled, textInput]);

  return (
    <span
      className={styles.chordForm}
      tabIndex={tabIndex}
      ref={textInput}
      onBlur={onSubmit}
      contentEditable={isEnabled}
      onKeyDown={onKeyDown}
      role="textbox"
      style={{ ...theme.typography.body2 }}
    />
  );
};
