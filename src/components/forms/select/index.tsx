import {
  FormControl,
  InputLabel,
  MenuItem,
  SelectProps as MUISelectProps,
  Select as MUISelect,
} from '@mui/material';
import { FC } from 'react';
// import { Dropdown, DropdownProps } from 'semantic-ui-react';

export interface SelectOption {
  label: string;
  value: string | number;
}

// export type SelectProps = DropdownProps;
export type SelectProps = {
  SelectProps?: MUISelectProps;
  options: SelectOption[];
  label: string;
};

export const Select: FC<SelectProps> = ({ SelectProps }) => {
  // return <Dropdown selection {...props} />;
  return (
    <FormControl fullWidth>
      <InputLabel id={SelectProps?.id}>Age</InputLabel>
      <MUISelect labelId={SelectProps?.id} {...SelectProps}>
        <MenuItem value={10}>Ten</MenuItem>
        <MenuItem value={20}>Twenty</MenuItem>
        <MenuItem value={30}>Thirty</MenuItem>
      </MUISelect>
    </FormControl>
  );
};

// placeholder="Song variants"
// value={chordVersionId}
// onChange={handleChordVersionChange}
// options={options}
// text={
//   chordVersionData?.author.username
//     ? `Author: ${chordVersionData?.author.username}`
//     : undefined
// }
