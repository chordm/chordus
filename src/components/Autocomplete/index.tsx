import { SyntheticEvent, useState } from 'react';
import { useBoolean, useDebounce } from 'utils/hooks';
import { Awaited } from 'utils/types';
import MuiAutocomplete, {
  AutocompleteInputChangeReason,
  AutocompleteProps as MuiAutocompleteProps,
} from '@mui/material/Autocomplete';

export type Suggestion = Record<string, any>;

export interface AutocompleteProps<Data extends Suggestion> {
  getSuggestions?: (text: string) => Promise<Data[]>;
  MuiAutocompleteProps: Omit<
    MuiAutocompleteProps<Data, false, true, true>,
    'options'
  >;
}

const defaultGetSuggestions = () => Promise.resolve([]);

export const Autocomplete = <S extends Suggestion>({
  getSuggestions = defaultGetSuggestions,
  MuiAutocompleteProps,
}: AutocompleteProps<S>) => {
  const [isLoading, { on: startLoading, off: stopLoading }] = useBoolean();
  const [suggestions, setSuggestions] = useState<
    Awaited<ReturnType<typeof getSuggestions>>
  >([]);

  const onSearch = useDebounce(async (text: string) => {
    if (text.length <= 2) return;
    startLoading();
    const data = await getSuggestions(text);
    setSuggestions(data);
    stopLoading();
  }, 250);

  const onInputChange = (
    event: SyntheticEvent<Element, Event>,
    text: string,
    reason: AutocompleteInputChangeReason,
  ) => {
    MuiAutocompleteProps.onInputChange?.(event, text, reason);
    const trimmedText = text.trim();
    if (trimmedText.length === 0) {
      setSuggestions([]);
      return;
    }
    onSearch(trimmedText);
  };

  return (
    <MuiAutocomplete
      id="Autocomplete"
      options={suggestions}
      loading={isLoading}
      {...MuiAutocompleteProps}
      onInputChange={onInputChange}
    />
  );
};
