import { GlobalSearchDto } from 'api/search';

export const isPerformer = (
  suggestion: Suggestion,
): suggestion is PerformerSuggestion => suggestion.type === 'performers';

export const isSong = (suggestion: Suggestion): suggestion is SongSuggestion =>
  suggestion.type === 'songs';

export type PerformerSuggestion = GlobalSearchDto['performers'][0] & {
  type: 'performers';
};
export type SongSuggestion = GlobalSearchDto['songs'][0] & { type: 'songs' };

export type Suggestion = PerformerSuggestion | SongSuggestion;

export const searchDtoToSuggestions = (searchDto: GlobalSearchDto) =>
  Object.entries(searchDto).flatMap<Suggestion>(([type, options]) =>
    options.map((o: GlobalSearchDto['performers' | 'songs']) => ({
      ...o,
      type,
    })),
  );

export const createSuggestionUrl = (url: any, id: any): string => {
  return `/${url}/${id}`;
};

export const getSuggestionData = (
  option: Suggestion,
): [string, string, string] => {
  if (isPerformer(option)) {
    return [option.bandName, 'performer', option.id] as const;
  } else {
    return [
      option.songTitle + ' - ' + option.performer.bandName,
      'song',
      String(option.id),
    ] as const;
  }
};
