import { FC } from 'react';
import { Box, Tooltip } from '@mui/material';
import TrashIcon from '@/public/icons/trash.svg';
import PlusIcon from '@/public/icons/plus.svg';
import { OutlinedIconButton } from 'components/OutlinedIconButton';

export enum ManagementItems {
  ADD = 'ADD',
  REMOVE = 'REMOVE',
}

interface ItemOptions {
  disabled?: boolean;
}

interface ChordManagerProps {
  onDelete?: () => void;
  onInsertNew?: () => void;
  itemsOptions?: Record<Partial<ManagementItems>, ItemOptions>;
}

export const ChordManager: FC<ChordManagerProps> = ({
  onDelete,
  onInsertNew,
  itemsOptions,
}) => (
  <Box component="aside">
    {[
      {
        onClick: onInsertNew,
        options: itemsOptions?.[ManagementItems.ADD],
        element: (
          <Tooltip
            title="Add new song section after this one"
            PopperProps={{ placement: 'right' }}
          >
            <OutlinedIconButton variant="outlined">
              <PlusIcon />
            </OutlinedIconButton>
          </Tooltip>
        ),
      },
      {
        onClick: onDelete,
        options: itemsOptions?.[ManagementItems.REMOVE],
        element: (
          <Tooltip
            title="Remove current song section"
            PopperProps={{ placement: 'right' }}
          >
            <OutlinedIconButton
              variant="outlined"
              disabled={itemsOptions?.[ManagementItems.REMOVE].disabled}
              sx={{
                path: {
                  stroke: theme =>
                    itemsOptions?.[ManagementItems.REMOVE].disabled
                      ? theme.palette.action.disabled
                      : undefined,
                },
              }}
            >
              <TrashIcon />
            </OutlinedIconButton>
          </Tooltip>
        ),
      },
    ].map((props, index) => (
      <Box
        aria-hidden="true"
        key={index}
        onClick={!props.options?.disabled ? props.onClick : undefined}
        sx={{ mb: 2 }}
      >
        {props.element}
      </Box>
    ))}
  </Box>
);
