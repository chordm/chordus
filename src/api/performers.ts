import { FetchItemsDto, FetchPerformersResponseDto } from 'api/dtos';
import { apiGatewayClient } from './clients';
import { Performer } from 'types';

export const getAll = async (fetchPerformersDto: FetchItemsDto<Performer>) => {
  const { data } = await apiGatewayClient.get<FetchPerformersResponseDto>(
    '/performer',
    { params: fetchPerformersDto },
  );
  return data;
};
