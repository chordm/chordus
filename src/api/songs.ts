import {
  CreateSongDto,
  FetchItemsDto,
  FetchSongsResponseDto,
  GetRatingDto,
} from 'api/dtos';
import { apiGatewayClient } from './clients';
import { Rating, Song } from 'types';

export const createSong = async (createSongDto: CreateSongDto) => {
  const { data } = await apiGatewayClient.post<Song>('/song', createSongDto);
  return data;
};

export const updateSong = async (
  songId: Song['id'],
  updateSongDto: CreateSongDto,
) => {
  const { data } = await apiGatewayClient.put<Song>(
    `/song/${songId}`,
    updateSongDto,
  );
  return data;
};

export const getAll = async (
  fetchSongDto: FetchItemsDto<Song>,
): Promise<FetchSongsResponseDto> => {
  const { data } = await apiGatewayClient.get<FetchSongsResponseDto>('/song', {
    params: fetchSongDto,
  });
  return new Promise(res => {
    setTimeout(() => res(data), 1000);
  });
  return data;
};

export const getOne = async (songId: number) => {
  const { data } = await apiGatewayClient.get<Song>(`/song/${songId}`);
  return new Promise(res => {
    setTimeout(() => res(data), 1000);
  });
  return data;
};

export const getSongVariants = async (songId: number) => {
  const { data } = await apiGatewayClient.get<Song[]>(
    `/song/${songId}/variants`,
  );
  return data;
};

export const addSongView = async (songId: number) => {
  const { data } = await apiGatewayClient.put(`/song/${songId}/views`);
  return data;
};

export const updateRating = async (songId: Song['id'], value: number) => {
  const { data } = await apiGatewayClient.put<Rating>(`/rating`, {
    songId,
    value,
  });
  return data;
};

export const getSongRating = async (songId: Song['id']) => {
  const { data } = await apiGatewayClient.get<GetRatingDto>(
    `/rating/${songId}`,
  );
  return data;
};
