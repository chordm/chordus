import { ClassValue } from 'clsx';
import { PropsWithChildren } from 'react';

export type ResponsiveLayoutProps = {
  rootClassName?: ClassValue;
};

export interface BaseLayoutProps
  extends ResponsiveLayoutProps,
    PropsWithChildren {
  logInWithGoogleHref: string;
  rootClassName?: ClassValue;
}
