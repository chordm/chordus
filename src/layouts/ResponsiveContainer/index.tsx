import { FC, PropsWithChildren } from 'react';
import { DesktopLayout } from 'layouts/DesktopLayout';

interface ResponsiveLayoutProps extends PropsWithChildren {}

export const ResponsiveContainer: FC<ResponsiveLayoutProps> = props => {
  const logInWithGoogleHref = [
    process.env.NEXT_PUBLIC_BASE_URL,
    process.env.NEXT_PUBLIC_GOOGLE_REDIRECT_URL,
  ].join('/');

  const layoutProps = { ...props, logInWithGoogleHref };

  return <DesktopLayout {...layoutProps} />;
};
