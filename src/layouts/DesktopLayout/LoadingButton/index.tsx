import { Button, Typography } from '@mui/material';
import { FC } from 'react';
import styles from './index.module.scss';

interface LoginButtonProps {
  logInWithGoogleHref: string;
}

export const LoginButton: FC<LoginButtonProps> = ({ logInWithGoogleHref }) => (
  <a href={logInWithGoogleHref} className={styles.loginButtonLink}>
    <Button
      className={styles.loginButton}
      sx={{ borderRadius: 100, p: 3.5, textTransform: 'none' }}
    >
      <Typography sx={{ pl: 1 }} variant="bold">
        Log in
      </Typography>
    </Button>
  </a>
);
