import { Box, BoxProps } from '@mui/material';
import { FC, PropsWithChildren } from 'react';

interface CenterDesktopLayoutProps extends PropsWithChildren {
  RootBoxProps?: BoxProps;
  ChildBoxProps?: BoxProps;
}

export const CenterDesktopLayout: FC<CenterDesktopLayoutProps> = props => {
  return (
    <Box
      {...props.RootBoxProps}
      sx={{
        width: '100%',
        ...props.RootBoxProps?.sx,
      }}
    >
      <Box
        {...props.ChildBoxProps}
        sx={{
          margin: 'auto',
          maxWidth: 900,
          ...props.ChildBoxProps?.sx,
        }}
      >
        {props.children}
      </Box>
    </Box>
  );
};
