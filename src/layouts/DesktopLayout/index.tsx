'use client';

import { FC } from 'react';
import { GlobalSearch } from 'components/GlobalSearch';
import Image from 'next/image';
import Link from 'next/link';
import { componentToPath, generatePath } from 'routing';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItemIcon from '@mui/material/ListItemIcon';
import { AddCircle, SignalCellularAlt } from '@mui/icons-material';
import { BaseLayoutProps } from 'layouts/ResponsiveContainer/types';
import { AppBar, LoginServerButton } from 'layouts/DesktopLayout/AppBar';
import ChordmLogoHeaderDesktopIcon from '@/public/icons/logos/chordmLogoHeaderDesktop.svg';
import { CenterDesktopLayout } from 'layouts/DesktopLayout/CenterDesktopLayout';
import { ListItemButton } from '@mui/material';

export const DesktopLayout: FC<BaseLayoutProps> = ({
  children,
  logInWithGoogleHref,
}) => {
  const drawer = (
    <div>
      <Toolbar>
        <Link href={componentToPath.TopChords}>
          <Image
            alt="Logo"
            src="/icons/logoSmall.svg"
            width={100}
            height={50}
          />
        </Link>
      </Toolbar>
      <Divider />
      <List>
        {[
          {
            label: 'Top Chords',
            link: componentToPath.TopChords,
            Icon: SignalCellularAlt,
          },
          {
            label: 'Submit song',
            link: generatePath.SubmitSong(),
            Icon: AddCircle,
          },
        ].map(({ label, link, Icon }) => (
          <Link href={link} key={link}>
            <ListItemButton>
              <ListItemIcon>
                <Icon />
              </ListItemIcon>
              {label}
            </ListItemButton>
          </Link>
        ))}
      </List>
    </div>
  );

  return (
    <Box sx={{ display: 'flex', mt: 24 }}>
      <AppBar
        position="fixed"
        drawer={drawer}
        sx={{
          background: theme => theme.palette.background.default,
          boxShadow: 'none',
          borderBottom: theme => `1px solid ${theme.palette.secondary.main}`,
          height: theme => theme.spacing(18),
          padding: '12px',
        }}
      >
        <CenterDesktopLayout
          ChildBoxProps={{
            sx: {
              flexDirection: 'row',
              display: 'flex',
              alignItems: 'center',
              gap: 6,
            },
          }}
        >
          <Link href="/" style={{ height: 32 }}>
            <ChordmLogoHeaderDesktopIcon />
          </Link>
          <GlobalSearch />
          <div
            style={{
              display: 'flex',
              flexWrap: 'nowrap',
              alignItems: 'center',
            }}
          >
            <LoginServerButton logInWithGoogleHref={logInWithGoogleHref} />
            {/* <IconButton
              color="inherit"
              aria-label="open drawer"
              // onClick={toggleDrawer}
              edge="start"
            >
              <MenuIcon />
            </IconButton> */}
          </div>
        </CenterDesktopLayout>
      </AppBar>
      {/* <Main open={true}> */}
      {/* <Main open={isDrawerOpened}> */}
      {/* <DrawerHeader /> */}
      {children}
      {/* </Main> */}
    </Box>
  );
};
