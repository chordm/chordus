import { ChordKeys } from 'types';

export const rhythmLinesMocks = [
  [
    [null, { key: ChordKeys.D }, null, null, null],
    [{ key: ChordKeys.D, body: '#m' }, null, null, null],
  ],
  [[null, { key: ChordKeys.A }, null, null, null]],
];
