import { v4 as uuidv4 } from 'uuid';

export const wordLinesMocks = [
  [
    {
      word: 'Hey,',
      chordAlign: { lineIndex: 0, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'you',
      chordAlign: { lineIndex: 0, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'there.',
      chordAlign: { lineIndex: 0, tactIndex: 1, chordIndex: 0 },
      id: uuidv4(),
    },
  ],
  [
    {
      word: 'Can',
      chordAlign: { lineIndex: 1, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'we',
      chordAlign: { lineIndex: 1, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'take',
      chordAlign: { lineIndex: 1, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'it',
      chordAlign: { lineIndex: 1, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'to',
      chordAlign: { lineIndex: 1, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'the',
      chordAlign: { lineIndex: 1, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'next',
      chordAlign: { lineIndex: 1, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
    {
      word: 'level',
      chordAlign: { lineIndex: 0, tactIndex: 0, chordIndex: 1 },
      id: uuidv4(),
    },
  ],
];
