/**
 * es - ♭
 * is - #
 * dur - major
 * moll - minor
 */

export enum ChordKeys {
  A = 'A',
  B = 'B',
  C = 'C',
  D = 'D',
  E = 'E',
  F = 'F',
  G = 'G',
}

export interface Chord {
  key: ChordKeys;
  body?: string;
  tonic?: ChordKeys;
}

export type ChordAlign = {
  lineIndex: number;
  tactIndex: number;
  chordIndex: number;
};

export type ChordItem = {
  word: string;
  chordAlign: ChordAlign;
  id: string;
};

export type ChordString = ChordItem[];
export type ChordPanel = ChordString[];
export type ChordPanels = ChordPanel[];

export type RhythmTact = (Chord | null)[];
export type RhythmLine = RhythmTact[];
export type RhythmPanel = RhythmLine[];
export type RhythmPanels = RhythmPanel[];

export interface Performer {
  id: string;
  bandName: string;
  views: number;
  averageRating: number | null;
}

export interface Rating {
  value: number;
  id: number;
  createdAt: string;
}

export interface Song {
  id: number;
  rhythmPanels: RhythmPanels;
  chordPanels: ChordPanels;
  titles: string[];
  rhythm: [number, number];
  songTitle: string;
  performer: Performer;
  author: User;
  views: number;
  averageRating: number;
}

export interface User {
  id: string | null;
  username: string;
}
