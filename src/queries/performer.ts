import { useInfiniteQuery } from '@tanstack/react-query';
import API from 'api';
import { DEFAULT_PAGE_SIZE } from 'constant';
import { getNextPageParam } from 'queries/utils';
import { TopVariant } from 'screens/TopChords/types';
import { Performer } from 'types';

export const usePerformers = (
  orderBy: keyof Performer,
  selectedTopVariant: TopVariant,
  pageSize = DEFAULT_PAGE_SIZE,
) =>
  useInfiniteQuery({
    queryKey: ['performers', { orderBy }],
    queryFn: ({ pageParam }) =>
      API.performers.getAll({ orderBy, ...pageParam }),
    enabled: selectedTopVariant === TopVariant.performer,
    initialPageParam: { top: pageSize, skip: 0 },
    getNextPageParam: (lastPage, pages) => getNextPageParam(pages),
  });
