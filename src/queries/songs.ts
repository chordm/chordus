import { CreateSongDto, GetRatingDto } from 'api/dtos';
import API from 'api';
import {
  useQuery,
  useMutation,
  UseQueryResult,
  useQueryClient,
  useInfiniteQuery,
} from '@tanstack/react-query';
import { produce } from 'immer';
import { DEFAULT_PAGE_SIZE } from 'constant';
import { createSong, updateSong } from 'api/songs';
import { useRouter } from 'next/navigation';
import { generatePath } from 'routing';
import getQueryClient from 'utils/getQueryClient';
import {
  TopVariant,
  defaultSorting,
  defaultTopVariant,
  sorting,
} from 'screens/TopChords/types';
import { Rating, Song } from 'types';
import { getNextPageParam } from 'queries/utils';

export const useUserChordRating = (songId?: Song['id']) =>
  useQuery({
    queryKey: ['songRating', { songId }],
    queryFn: () => API.songs.getSongRating(songId!),
    enabled: typeof songId !== 'undefined',
  });

export const useIncrementSongViews = () =>
  useMutation({
    mutationFn: (songId: Song['id']) => API.songs.addSongView(songId),
  });

export interface UpdateRatingVariables {
  songId: Song['id'];
  rating: Song['averageRating'];
}

export interface UpdateRatingContext {
  previousRating?: GetRatingDto;
}

export const useUpdateUserChordRating = () => {
  const queryClient = useQueryClient();

  return useMutation<Rating, Error, UpdateRatingVariables, UpdateRatingContext>(
    {
      mutationFn: ({ rating, songId }) =>
        API.songs.updateRating(songId, rating),
      onMutate: async ({ songId, rating }) => {
        await queryClient.cancelQueries({
          queryKey: ['songRating', { songId }],
        });
        const previousRating = queryClient.getQueryData<GetRatingDto>([
          'songRating',
          { songId },
        ]);
        if (!previousRating?.current?.value) return { previousRating };
        const updatedRating = produce(previousRating, draft => {
          if (!draft.current) return;
          draft.current.value = rating;
        });
        queryClient.setQueryData(['songRating', { songId }], updatedRating);
        return { previousRating };
      },
      onSettled: (data, err, { songId }) => {
        queryClient.invalidateQueries({
          queryKey: ['songRating', { songId }],
        });
      },
      onError: (err, { songId }, context) => {
        queryClient.setQueryData(
          ['songRating', { songId }],
          context?.previousRating,
        );
      },
    },
  );
};

export const useSongs = (
  orderBy: keyof Song,
  selectedTopVariant: TopVariant,
  pageSize = DEFAULT_PAGE_SIZE,
) =>
  useInfiniteQuery({
    queryKey: ['songs', { orderBy }],
    queryFn: ({ pageParam }) => API.songs.getAll({ orderBy, ...pageParam }),
    enabled: selectedTopVariant === TopVariant.chord,
    getNextPageParam: (_lastPage, pages) => getNextPageParam(pages),
    initialPageParam: { top: pageSize, skip: 0 },
  });

export const usePrefetchSongs = async () => {
  const queryClient = getQueryClient();
  const defaultSortingOption = defaultSorting[defaultTopVariant];
  const defaultSortingData = sorting[defaultTopVariant].find(
    option => option.key === defaultSortingOption,
  );
  if (!defaultSortingData) {
    throw new Error('Default sorting is not denied');
  }
  await queryClient.prefetchInfiniteQuery({
    queryKey: ['songs', { orderBy: defaultSortingData.property }],
    queryFn: ({ pageParam }) =>
      API.songs.getAll({
        orderBy: defaultSortingData.property as keyof Song,
        ...pageParam,
      }),
    initialPageParam: { top: DEFAULT_PAGE_SIZE, skip: 0 },
  });
};

export const useSong = (
  songId?: Song['id'],
): UseQueryResult<Song | undefined> =>
  useQuery({
    queryKey: ['song', { songId }],
    queryFn: () => API.songs.getOne(songId!),
    enabled: typeof songId !== 'undefined',
  });

export const usePrefetchSong = async (songId?: Song['id']) => {
  const queryClient = getQueryClient();
  if (typeof songId !== 'number') return undefined;
  await queryClient.prefetchQuery({
    queryKey: ['song', { songId }],
    queryFn: () => API.songs.getOne(songId),
  });
};

export const useSongVariants = (
  songId?: Song['id'],
): UseQueryResult<Song[] | undefined> =>
  useQuery({
    queryKey: ['songVariants', { songId }],
    queryFn: () => API.songs.getSongVariants(songId!),
    enabled: typeof songId !== 'undefined',
  });

export const useCreateSong = () => {
  const router = useRouter();

  return useMutation({
    mutationFn: (song: CreateSongDto) => createSong(song),
    onSuccess: song => {
      router.push(generatePath.Chords(song.id));
    },
  });
};

export const useUpdateSong = () => {
  const router = useRouter();

  return useMutation({
    mutationFn: (options: { songId: Song['id']; songDto: CreateSongDto }) =>
      updateSong(options.songId, options.songDto),
    onSuccess: song => {
      router.push(generatePath.Chords(song.id));
    },
  });
};
