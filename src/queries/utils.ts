import { DEFAULT_PAGE_SIZE } from 'constant';
import { FetchPerformersResponseDto, FetchSongsResponseDto } from 'api/dtos';

export const getNextPageParam = (
  pages: (FetchSongsResponseDto | FetchPerformersResponseDto)[] = [],
) => {
  const lastPage = pages[pages.length - 1];
  if (!lastPage) return undefined;
  const total = lastPage?.totalCount ?? Number.MAX_SAFE_INTEGER;
  const downloaded = pages.reduce(
    (count, { items }) => count + items.length,
    0,
  );
  const itemsLeft = total - downloaded;
  if (itemsLeft <= 0) return undefined;
  const itemsToLoad = Math.min(itemsLeft, DEFAULT_PAGE_SIZE);

  return { skip: downloaded, top: itemsToLoad };
};
